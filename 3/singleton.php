<?php

class DB
{
	private static $_instance = null; // информация о соединении
	
	
	private static DB_HOST = ''; //Имя хоста(номер порта)
	private static DB_NAME = ''; //Имя базы данных
	private static DB_USER = ''; //Имя пользователя для доступа к базе данных
	private static DB_PASS = ''; //Пароль пользователя

	private function __construct () {
		
		$this->_instance = new PDO( //отрабатывает один раз при вызове getInstance();
			'mysql:host=' . self::DB_HOST . ';dbname=' . self::DB_NAME, self::DB_USER, self::DB_PASS, [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]
	    );

	}

	private function __clone () {} //запрещаем клонирование объекта модификатором private
	private function __wakeup () {} //запрещаем клонирование объекта модификатором private

	public static function getInstance()
	{
		if (self::$_instance != null) { //проверяем есть ли что-то в $_instance, если есть, то возращаем. Если нет, то создаем через new self
			return self::$_instance;
		}

		return new self;
	}
}
?>

<?php

$db = DB::getInstance(); // инициализация экземпляра класса для работы с БД

?>