<?php

trait Trait1{
	private function methot1(){
		return 1;
	}
}

trait Trait2{

	private function methot2(){
		return 2;
	}
}

trait Trait3{
	private function methot3(){
		return 3;
	}
}



class Test
{
	use Trait1, Trait2, Trait3;
 

	public function getSum(){
		$a = $this->methot1();
		$b = $this->methot2();
		$c = $this->methot3();
		$sum = $a + $b + $c;
		return $sum;
	}
}

$test = new Test;
$test->getSum();


?>