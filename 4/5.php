<?php


class ABC{

  private $str;
    function __construct()
    {
        $this->str = "";
    }

    function addA()
    {
        $this->str .= "a";
        return $this;
    }

    function addB()
    {
        $this->str .= "b";
        return $this;
    }

    function addC()
    {
        $this->str .= "c";
        return $this;
    }

    function getStr()
    {
        return $this->str;
    }
}


$a = new ABC();
echo $a->addA()->addB()->addC()->getStr();


?>