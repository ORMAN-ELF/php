<?php

class Car{

    public $color; 
    public $year; 
    public $manufacture;
    public $switch;

    public function __construct($color, $year, $manufacture){
        $this->switch = new Engine();
        $this->color = $color;
        $this->year = $year;
        $this->manufacture = $manufacture;
    }

    public function start_engine(){
        $this->switch->on();
    }
    public function stop_engine(){
        $this->switch->off();
    } 
}

class Engine{

    public function on(){
        echo 'on';
    }

    public function off(){
        echo 'off';
    }
}

$car = new Car('black', 3, 'Russia');
$car->start_engine();
$car->stop_engine();


?>