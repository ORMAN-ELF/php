<?php

class Order {

    public function calculateTotalSum(){/*...*/}
    public function getItems(){/*...*/}
    public function getItemCount(){/*...*/}
    public function addItem($item){/*...*/}
    public function deleteItem($item){/*...*/}
}

class fileOrder extends Order {

    public function printOrder(){/*...*/}
    public function showOrder(){/*...*/}
}

class actionsOrder extends Order {

    public function load(){/*...*/}
    public function save(){/*...*/}
    public function update(){/*...*/}
    public function delete(){/*...*/}
}

?>