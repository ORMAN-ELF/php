<?php

interface forFactory {
    public function getProductFirst();
    public function getProductSecond(); 
    public function getProductThird();
}

interface forProduct {
    public function getName();
}


class Factory implements forFactory {

    public function getProductFirst() {

        return new FirstProduct();
    }

    public function getProductSecond() {

        return new SecondProduct();
    }

    public function getProductThird() {

        return new ThirdProduct();
    }
}


class FirstProduct implements forProduct {

    public function getName() {
        return 'The first product';
    }
}


class SecondProduct implements forProduct {

    public function getName() {
        return 'The second product';
    }
}

class ThirdProduct implements forProduct {

    public function getName() {
        return 'The third product';
    }
}

$factory = new Factory();
$firstProduct = $factory->getProductFirst();
$secondProduct = $factory->getProductSecond();
$thirdProduct = $factory->getProductThird();

print_r($firstProduct->getName().'<br>');
print_r($secondProduct->getName().'<br>');
print_r($thirdProduct->getName());

?>