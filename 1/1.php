<?php

  class Students {
    var $name = "Имя";
    var $age = "Возраст";
    var $salary = "Стипендия";
    
  }

  $student = new Students;
  $student->name="Anton";
  $student->age=25;
  $student->salary=1100;

  $student2 = new Students;
  $student2->name = "Oleg";
  $student2->age = 26;
  $student2->salary = 2200;

  echo 'Sum age: ' . ($student->age + $student2->age) . '<br>';
  echo 'Sum salary: ' . ($student->salary + $student2->salary);

?>