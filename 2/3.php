<?php

interface iUser {
    public function __construct($name, $age){
      function getName();
      function getAge();
    } 
  }

interface iEmployee extends iUser{
    public function setSalary($salary);
    public function getSalary();
  }
  

class Employee implements iEmployee{

  public $salary;

  public function setSalary($salary){
        $this->salary = $salary;
    }

  public function getSalary(){
        return $this->salary;
   }
}

?>